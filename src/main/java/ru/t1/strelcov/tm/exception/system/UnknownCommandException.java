package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error: Unknown command. Use \"help\" command.");
    }

}
