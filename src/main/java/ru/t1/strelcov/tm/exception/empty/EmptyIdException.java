package ru.t1.strelcov.tm.exception.empty;

import ru.t1.strelcov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error: Id is empty.");
    }

    public EmptyIdException(final String value) {
        super("Error: " + value + " id is empty.");
    }

}
